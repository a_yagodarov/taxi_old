<?php

namespace app\controllers;

use app\models\Tariff;
use app\models\TransferTariffMin;
use app\models\User;
use Yii;
use app\models\Transfer;
use app\models\TransferSearch;
use yii\web\Response;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransferController implements the CRUD actions for Transfer model.
 */
class TransferController extends Controller
{
    /**
     * @inheritdoc
     */

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete'  => ['post'],
					'confirm' => ['post'],
					'block'   => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'ruleConfig' => [
					'class' => AccessRule::className(),
				],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
						'actions' => [
							'transfer-price',
						],
						'matchCallback' => function(){
							return in_array(Yii::$app->user->identity->role_id, [
								User::LEGAL_ENTITY,
								User::INDIVIDUAL
							]);
						}
					],
					[
						'allow' => true,
						'roles' => ['@'],
						'matchCallback' => function(){
							return in_array(Yii::$app->user->identity->role_id, [
								User::ROLE_ADMIN
							]);
						}
					],
				],
			],
		];
	}

    /**
     * Lists all Transfer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Transfer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transfer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transfer();
	    $model->loadTariffs();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        $tariff = $_POST['TransferTariffMin'];
	        $model->saveTariffs($tariff);
	        return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Transfer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	    $model = $this->findModel($id);
	    $model->loadTariffs();
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    $tariff = $_POST['TransferTariffMin'];
		    $model->saveTariffs($tariff);
		    return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Transfer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transfer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transfer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transfer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionTransferPrice()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];
		$tariff_id = $_POST['tariff'];
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Transfer::getPrice($from, $to, $tariff_id);
	}
}
