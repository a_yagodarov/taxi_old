<?php

namespace app\controllers;

use app\models\UploadForm;
use app\models\User;
use app\models\UserInfo;
use kartik\mpdf\Pdf;
use Yii;
use app\models\Payment;
use app\models\PaymentSearch;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'ruleConfig' => [
			        'class' => AccessRule::className(),
		        ],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'actions' => ['index', 'get-pdf', 'change-status', 'bill'],
				        'matchCallback' => function(){
					        return (Yii::$app->user->identity->role_id == User::ROLE_ADMIN);
				        }
			        ],
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'actions' => ['index', 'create', 'bill'],
				        'matchCallback' => function(){
					        return (in_array(Yii::$app->user->identity->role_id,[
						        User::LEGAL_ENTITY,
						        User::INDIVIDUAL,
					        ]));
				        }
			        ],
		        ],
	        ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    switch (Yii::$app->user->identity->role_id){
		    case User::ROLE_ADMIN:
		    {
			    return $this->render('admin_index', [
				    'searchModel' => $searchModel,
				    'dataProvider' => $dataProvider,
			    ]);
		    }
		    case User::LEGAL_ENTITY :
			    case User::INDIVIDUAL:
				    return $this->render('index', [
					    'searchModel' => $searchModel,
					    'dataProvider' => $dataProvider,
				    ]);
	    }
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();
	    $model->scenario = 'create';
//		$uploadModel = new UploadForm();
        if ($model->load(Yii::$app->request->post())) {
	        $model->link_pdf = UploadedFile::getInstance($model, 'link_pdf');
	        $model->client_id = Yii::$app->user->identity->getId();
	        $model->status = 0;
//	        $model->file = null;
	        if ($model->save()) {
		        return $this->redirect('index');
	        }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

//	public function actionGetPdf()
//	{
//		$path = Yii::getAlias('@app/files/pdf/');
//		$id = $_GET['id'];
//		$filename = (new Query())
//			->select('link_pdf')
//			->from(Payment::tableName())
//			->where(['id' => $id])
//			->one()['link_pdf'];
//
//		$file = $path . $filename;
//		if (file_exists($file))
//			Yii::$app->response->sendFile($file);
//	}

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

	public function actionBill()
	{
		$payment = Payment::findOne(['id' => $_GET['id']]);
		$user_info = UserInfo::findOne(['id' => $payment['client_id']]);
		$content = $this->renderPartial('bill', [
			'payment' => $payment,
			'user_info' => $user_info
		]);
		$pdf = new Pdf([
			// set to use core fonts only
			'mode' => Pdf::MODE_UTF8,
			// A4 paper format
			'format' => Pdf::FORMAT_A4,
			// portrait orientation
			'orientation' => Pdf::ORIENT_PORTRAIT,
			// stream to browser inline
			'destination' => Pdf::DEST_BROWSER,
			// your html content input
			'content' => $content,
			// format content from your own css file if needed or use the
			// enhanced bootstrap css built by Krajee for mPDF formatting
			'cssFile' => '@app/web/css/bill.css',
			// any css to be embedded if required
			'cssInline' => '.kv-heading-1{font-size:18px}',
			// set mPDF properties on the fly
			'options' => ['title' => 'Счет на оплату'],
			// call mPDF methods on the fly
			'methods' => [
//				'SetHeader'=>['Krajee Report Header'],
//				'SetFooter'=>['{PAGENO}'],
			]
		]);
		return $pdf->render();
	}

	public function actionChangeStatus()
	{
		$id  = $_POST['id'];
		$status = $_POST['status'];
		$result = Payment::findOne(['id' => $id])->changeStatus($status);
		Yii::$app->response->format = Response::FORMAT_JSON;
		return $result;
	}

    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
