<?php

namespace app\controllers;

use app\models\DriverCar;
use app\models\Order;
use app\models\User;
use dektrium\user\filters\AccessRule;
use Yii;
use app\models\Driver;
use app\models\DriverSearch;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DriverController implements the CRUD actions for Driver model.
 */
class DriverController extends Controller
{
    /**
     * @inheritdoc
     */

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete'  => ['post'],
					'confirm' => ['post'],
					'block'   => ['post'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'ruleConfig' => [
					'class' => AccessRule::className(),
				],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
						'actions' => [
							'get-drop-down'
						],
						'matchCallback' => function(){
							return in_array(Yii::$app->user->identity->role_id, [
								User::ROLE_MANAGER,
								User::ROLE_ACCOUNTANT,
								User::ROLE_DISPATCHER,
							]);
						}
					],
					[
						'allow' => true,
						'roles' => ['@'],
//						'actions' => ['@'],
						'matchCallback' => function(){
							return in_array(Yii::$app->user->identity->role_id, [
								User::ROLE_ADMIN
							]);
						}
					],
				],
			],
		];
	}

    /**
     * Lists all Driver models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Driver model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Driver model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Driver();
		$model->loadCars();
//	    $modelCar = [new DriverCar()];
        if ($model->load(Yii::$app->request->post())) {
	        if ($model->save())
	        {
		        $model->saveCars($_POST['DriverCar']);
		        return $this->redirect('index');
	        }
        } else {
            return $this->render('create', [
                'model' => $model,
//	            'modelDriverCar' => $modelDriverCar
            ]);
        }
    }

    /**
     * Updates an existing Driver model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$model->loadCars();
        if ($model->load(Yii::$app->request->post())) {
	        if ($model->save())
	        {
		        $model->saveCars($_POST['DriverCar']);
		        return $this->redirect('index');
	        }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Driver model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Driver model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionGetDropDown()
	{
		if (Yii::$app->request->isAjax)
		{
			$id = $_POST['id'];

			$order_id = $_POST['order_id'];
			$order = Order::findOne(['id' => $order_id]);

			$driver = Driver::findOne(['id' => $id]);
			$driverCars = $driver->getDriverCars($id);
			$return = '<option value="">Выберите авто</option>';
			foreach ($driverCars as $key => $item)
			{
				if ($key == $order->car_id)
				{
					$return .= '<option value='.$key.' selected>'.$item.'</option>';
				}
				else
					$return .= '<option value='.$key.'>'.$item.'</option>';
			}
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $return;
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return [];
	}
}
