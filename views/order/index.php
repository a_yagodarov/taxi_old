<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;
//use kartik\editable\Editable;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$css = <<< CSS
thead select {
    width: 80px;
}
.form-control {
	   width: 100%;
    height: 25px;
    padding: 0;
}
tr > th {
	padding:0px !important;
}
CSS;
$urlGetCars = \yii\helpers\Url::to('/driver/get-drop-down');
$urlSendSms = \yii\helpers\Url::to('/order/send-sms');
$urlSendEmail = \yii\helpers\Url::to('/order/send-email');
$ajaxUrl = \yii\helpers\Url::to('/order/index');
$js = <<< JS
$('select').addClass('form-control');
function replaceCarsDropDown(item_id)
{
	$.ajax({
		url : '$urlGetCars',
		type : 'post',
		data : {
			id : $('select[name="driver_id"][item_id="'+item_id+'"]').val(),
			order_id: item_id
		},
		success : function(data){
			$('select[name="car_id"][item_id="'+item_id+'"]').empty();
			$('select[name="car_id"][item_id="'+item_id+'"]').append(data);
		}
	})
}
$('tbody select').on('input', function(){
	var item_id = $(this).attr('item_id');
	var status_id = $('select[name="status_id"][item_id="'+item_id+'"]').val();
	var car_id = $('select[name="car_id"][item_id="'+item_id+'"]').val();
	if ($(this).attr('name') == 'driver_id') replaceCarsDropDown(item_id);
	var driver_id = $('select[name="driver_id"][item_id="'+item_id+'"]').val();
	console.log(status);
	$.ajax({
		url : '$ajaxUrl',
		type: 'post',
		data : {
			id : item_id,
			status_id : status_id,
			car_id : car_id,
			driver_id : driver_id,
		}
	});
});
$('.send_sms').click(function(){
	var order_id = $(this).attr('order_id');
	$(item).removeClass('button-success');
	$(item).removeClass('button-fail');
	$(this).addClass('button-click');
	$(item).next().next().addClass('hidden');
	$(item).next().next().removeClass('button-success');
	$(item).next().next().removeClass('button-fail');
	var item = this;
	$.ajax({
		url: '$urlSendSms'+'?id='+order_id,
		type: 'get',
		success: function(data){
			console.log(data);
			if (data == true)
			{
				$(item).next().next().addClass('button-success');
				$(item).next().next().removeClass('hidden');
				$(item).next().next().html("Отправлено");
				$(item).removeClass('button-click');
				$(item).addClass('button-success');
			}
			else
			{
				$(item).next().next().addClass('button-fail');
				$(item).next().next().removeClass('hidden');
				$(item).next().next().html(data);
				$(item).removeClass('button-click');
				$(item).addClass('button-fail');
			}

		}
	}).fail(function(){
		$(this).removeClass('button-click');
		$(this).addClass('button-fail');
	});
});
$('.send_email').click(function(){
	var order_id = $(this).attr('order_id');
	$(item).removeClass('button-success');
	$(item).removeClass('button-fail');
	$(this).addClass('button-click');
	$(item).next().next().addClass('hidden');
	$(item).next().next().removeClass('button-success');
	$(item).next().next().removeClass('button-fail');
	var item = this;
	$.ajax({
		url: '$urlSendEmail'+'?id='+order_id,
		type: 'get',
		success: function(data){
			console.log(data);
			if (data == true)
			{
				$(item).next().next().addClass('button-success');
				$(item).next().next().removeClass('hidden');
				$(item).next().next().html("Отправлено");
				$(item).removeClass('button-click');
				$(item).addClass('button-success');
			}
			else
			{
				$(item).next().next().addClass('button-fail');
				$(item).next().next().removeClass('hidden');
				$(item).next().next().html(data);
				$(item).removeClass('button-click');
				$(item).addClass('button-fail');
			}

		}
	}).fail(function(){
		$(this).removeClass('button-click');
		$(this).addClass('button-fail');
	});
})
JS;
$jsBody = <<< JS
$('body').addClass('sidebar-collapse');
JS;


$this->registerCss($css);
$this->registerJs($js);
$this->registerJs($jsBody, \yii\web\View::POS_END);
$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
	        [
		        'attribute' => 'datetime_booking',
		        'format' => 'raw',
		        'value' => function($model){
			        return date('d.m.Y', strtotime($model->datetime_booking)).'<br>'.date('H:i', strtotime($model->datetime_booking));
		        },
		        'contentOptions' => [
			        'style' => 'width: 10%;'
		        ]
	        ],
	        [
		        'attribute' => 'from_point_id',
		        'label' => 'Откуда',
		        'value' => function($model)
		        {
			        return \app\models\Transfer::getPointById($model->from_point_id);
		        },
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'from_point_id',
			        \app\models\Transfer::getPointsForDropDownList(),
			        [
				        'prompt' => Yii::t('app', 'Choose')
			        ]),
		        'contentOptions' => [
			        'style' => 'max-width: 15%;width: 10%;'
		        ]
	        ],
	        [
		        'attribute' => 'to_point_id',
		        'label' => 'Куда',
		        'value' => function($model)
		        {
			        return \app\models\Transfer::getPointById($model->to_point_id);
		        },
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'to_point_id',
			        \app\models\Transfer::getPointsForDropDownList(),
			        [
				        'prompt' => Yii::t('app', 'Choose')
			        ]),
		        'contentOptions' => [
			        'style' => 'max-width: 15%;width: 10%;'
		        ]
	        ],
	        [
		        'attribute' => 'client_id',
		        'format' => 'raw',
		        'value' => function($model)
		        {
			        $arr = \app\models\Client::getUserInfo($model->client_id);
			        $echo = $arr['title'];
			        return Html::a($echo, \yii\helpers\Url::toRoute(['client/update', 'id' => $model->client_id]));
		        },
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'client_id',
			        \app\models\Client::getClientsForDropDownList(),
			        [
				        'prompt' => Yii::t('app', 'Choose')
			        ]),
		        'contentOptions' => [
			        'style' => 'max-width: 15%;width: 5%;'
		        ]
	        ],
	        [
		        'attribute' => 'car_id',
		        'format' => 'raw',
		        'label' => 'Обслуживание',
		        'value' => function($model) {
			        return
				        Html::tag('div', '<b>Класс: </b>'.$model->getTariff()->one()['name']).
			            Html::tag('div', '<b>Пассажиры: </b>'.$model->passengers).
			            Html::tag('div', '<b>Комментарий: </b>'.$model->comment);
		        },
		        'contentOptions'=> [
			        'style'=>'width:15%'
		        ],
		        'filter' => false
	        ],

	        [
		        'attribute' => 'price',
		        'contentOptions'=> [
			        'style'=>'width:5%'
		        ],
                'value' => function($model) {
	                $return = $model->price ? $model->price.'р.' : null;
	                return $return;
                }
//		        'label' => 'Стоимость',
	        ],
	        [
		        'attribute' => 'driver_id',
		        'label' => 'Водитель, авто',
		        'format' => 'raw',
		        'value' => function($model){
			        $driver = \app\models\Driver::findOne($model->driver_id);
			        return
			            '<strong>Водитель: </strong>'.Html::dropDownList('driver_id', $model->driver_id, \app\models\Driver::getDriversForDropdownList(), [
				            'item_id' => $model->id,
				            'prompt' => '--'
			            ]).
				        Html::tag('br', '', ['style' => 'margin:5px']).
			            '<strong>Автомобиль: </strong>'.Html::dropDownList('car_id', $model->car_id, \app\models\Car::getCarsForDropDownList(), [
				            'item_id' => $model->id,
				            'prompt' => '--'
			            ]).
			            Html::tag('div', '<strong>Номер: </strong>'.($driver['phone_number'] ? $driver['phone_number'] : $driver['phone_number_2']));
		        },
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'driver_id',
			        \app\models\Driver::getDriversForDropdownList(),
			        [
				        'prompt' => Yii::t('app', 'Choose')
			        ]),
		        'contentOptions'=> [
			        'style'=>'width:15%'
		        ],
	        ],
	        [
		        'attribute' => 'status_id',
		        'format' => 'raw',
		        'value' => function($model){
			        return
				        Html::dropDownList('status_id', $model->status_id, \app\models\Status::getStatusListForDropDownList(), [
				        'item_id' => $model->id
			        ])
				        .'<br>'.Html::button('Отправить смс', [
				        'order_id' => $model->id,
				        'class' => 'send_sms'
				        ])
				        .'<br><div class="hidden"></div>'
				        .'<br>'.Html::button('Отправить email', [
				        'order_id' => $model->id,
				        'class' => 'send_email'
			        ]).'<br><div class="hidden"></div>';
		        },
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'status_id',
			        \app\models\Status::getStatusListForDropDownList(),
			        [
				        'prompt' => Yii::t('app', 'Choose')
			        ]),
		        'contentOptions'=> [
			        'style'=>'width:12%'
		        ],
	        ],
            [
	            'class' => 'yii\grid\ActionColumn',
	            'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
