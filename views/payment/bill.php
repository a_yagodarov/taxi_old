<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 18.05.2016
 * Time: 0:31
 */
/* @var $this yii\web\View */
/* @var $user_info app\models\UserInfo */
/* @var $payment app\models\Payment */
function num2str($num) {
	$nul='ноль';
	$ten=array(
		array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
		array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
	);
	$a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
	$tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
	$hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
	$unit=array( // Units
		array('копейка' ,'копейки' ,'копеек',	 1),
		array('рубль'   ,'рубля'   ,'рублей'    ,0),
		array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
		array('миллион' ,'миллиона','миллионов' ,0),
		array('миллиард','милиарда','миллиардов',0),
	);
	//
	list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
	$out = array();
	if (intval($rub)>0) {
		foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
			if (!intval($v)) continue;
			$uk = sizeof($unit)-$uk-1; // unit key
			$gender = $unit[$uk][3];
			list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
			// mega-logic
			$out[] = $hundred[$i1]; # 1xx-9xx
			if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
			else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
			// units without rub & kop
			if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
		} //foreach
	}
	else $out[] = $nul;
	$out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
	$out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
	return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5) {
	$n = abs(intval($n)) % 100;
	if ($n>10 && $n<20) return $f5;
	$n = $n % 10;
	if ($n>1 && $n<5) return $f2;
	if ($n==1) return $f1;
	return $f5;
}
$months = [
	1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
	5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
	9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
];

$date = date('d', time()).' '.$months[date('n', time())].' '.date('Y', time());
$info = implode(', ', [
	$user_info->legal_name,
	$user_info->getAttributeLabel('inn').' '.$user_info->inn,
	$user_info->getAttributeLabel('kpp').' '.$user_info->kpp,
	$user_info->address
]);
$coeff = 0.152;
$nds = $payment->value * $coeff;
$sum = $payment->value - $nds;
?>
<!--<body style="background: #ff7701; margin: 0; font-family: Arial; font-size: 8pt; font-style: normal; ">-->
<?//= 'HELLO WORLD!' ?>
<table style="width:100%; height:0px; " cellspacing="0">
	<colgroup><col width="7">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="17">
		<col width="16">
		<col width="17">
		<col width="17">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col>
	</colgroup><tbody><tr class="R0">
		<td><div style="position:relative; height:15px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R0C1" colspan="32" rowspan="3"><div style="width:100%;height:44px;overflow:hidden;">Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате <br> обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту<br> прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.</div></td>
		<td><div style="width:100%;height:15px;overflow:hidden;"></div></td>
	</tr>
	<tr class="R0">
		<td><div style="position:relative; height:15px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:15px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R4">
		<td><span></span></td>
		<td class="R4C1" colspan="18" rowspan="2"><div style="width:100%;height:31px;overflow:hidden;">ФИЛИАЛ ОАО "УРАЛСИБ" В Г.УФА Г. УФА</div></td>
		<td class="R4C19" colspan="3"><span style="white-space:nowrap;max-width:0px;">БИК</span></td>
		<td class="R4C22" colspan="11"><span style="white-space:nowrap;max-width:0px;">048073770</span></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R5C19" colspan="3" rowspan="2"><div style="width:100%;height:29px;overflow:hidden;"><span style="white-space:nowrap;max-width:0px;">Сч.&nbsp;№</span></div></td>
		<td class="R5C22" colspan="11" rowspan="2"><div style="width:100%;height:29px;overflow:hidden;"><span style="white-space:nowrap;max-width:0px;">30101810600000000770</span></div></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R6C1" colspan="18"><span style="white-space:nowrap;max-width:0px;">Банк&nbsp;получателя</span></td>
		<td></td>
	</tr>
	<tr class="R4">
		<td><span></span></td>
		<td class="R7C1" colspan="2"><span style="white-space:nowrap;max-width:0px;">ИНН</span></td>
		<td class="R7C3" colspan="7"><span style="white-space:nowrap;max-width:0px;">0245951927</span></td>
		<td class="R7C1" colspan="2"><span style="white-space:nowrap;max-width:0px;">КПП&nbsp;&nbsp;</span></td>
		<td class="R7C3" colspan="7"><span style="white-space:nowrap;max-width:0px;">024501001</span></td>
		<td class="R7C19" colspan="3" rowspan="4"><div style="width:100%;height:61px;overflow:hidden;"><span style="white-space:nowrap;max-width:0px;">Сч.&nbsp;№</span></div></td>
		<td class="R7C19" colspan="11" rowspan="4"><div style="width:100%;height:61px;overflow:hidden;"><span style="white-space:nowrap;max-width:0px;">40702810900820002979</span></div></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R8C1" colspan="18" rowspan="2"><div style="width:100%;height:29px;overflow:hidden;">ООО "Трансфер Уфа"</div></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R10C1" colspan="18"><span style="white-space:nowrap;max-width:0px;">Получатель</span></td>
		<td></td>
	</tr>
	<tr>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R12C1" colspan="32" rowspan="2"><div style="width:100%;height:29px;overflow:hidden;"><span style="white-space:nowrap;max-width:0px;">Счет&nbsp;на&nbsp;оплату&nbsp;№&nbsp;<?= $payment->id ?>&nbsp;от&nbsp;<?=  $date?>&nbsp;г.</span></div></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1" colspan="32"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr class="R16">
		<td><span></span></td>
		<td class="R16C1" colspan="4"><span style="white-space:nowrap;max-width:0px;">Поставщик:</span></td>
		<td class="R16C5" colspan="28">ООО "Трансфер Уфа", ИНН 0245951927, КПП 024501001, 450501, Башкортостан Респ, Уфимский, С. Булгаково, Цюрупа, дом № 99</td>
		<td></td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr class="R16">
		<td><span></span></td>
		<td class="R16C1" colspan="4"><span style="white-space:nowrap;max-width:0px;">Покупатель:</span></td>
		<td class="R16C5" colspan="28"><?= $info ?></td>
		<td></td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	</tbody></table>
<table style="width:100%; height:0px; " cellspacing="0">
	<colgroup><col width="7">
		<col width="32">
		<col width="329">
		<col width="54">
		<col width="42">
		<col width="87">
		<col width="100">
		<col>
	</colgroup><tbody><tr class="R4">
		<td><span></span></td>
		<td class="R20C1"><span style="white-space:nowrap;max-width:0px;">№</span></td>
		<td class="R20C2"><span style="white-space:nowrap;max-width:0px;">Товары&nbsp;(работы,&nbsp;услуги)</span></td>
		<td class="R20C2"><span style="white-space:nowrap;max-width:0px;">Кол-во</span></td>
		<td class="R20C2"><span style="white-space:nowrap;max-width:0px;">Ед.</span></td>
		<td class="R20C2"><span style="white-space:nowrap;max-width:0px;">Цена</span></td>
		<td class="R20C6"><span style="white-space:nowrap;max-width:0px;">Сумма</span></td>
		<td><span></span></td>
		<td></td>
	</tr>
	<tr class="R0">
		<td><span></span></td>
		<td class="R21C1"><span style="white-space:nowrap;max-width:0px;">1</span></td>
		<td class="R21C2">Услуги легкового такси</td>
		<td class="R21C3"><span style="white-space:nowrap;max-width:0px;">1</span></td>
		<td class="R21C4"><span style="white-space:nowrap;max-width:0px;">шт</span></td>
		<td class="R21C3"><span style="white-space:nowrap;max-width:0px;"><?= number_format($nds, 2, '.', ' ')?></span></td>
		<td class="R21C6"><span style="white-space:nowrap;max-width:0px;"><?= number_format($nds, 2, '.', ' ')?></span></td>
		<td><span></span></td>
		<td></td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R22C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr class="R4">
		<td class="R23C5" colspan="6"><span style="white-space:nowrap;max-width:0px;">Итого:</span></td>
		<td class="R23C5"><span style="white-space:nowrap;max-width:0px;"><?= number_format($sum, 2, '.', ' ') ?></span></td>
		<td><span></span></td>
		<td></td>
	</tr>
	<tr class="R4">
		<td class="R23C5" colspan="6"><span style="white-space:nowrap;max-width:0px;">Сумма&nbsp;НДС:</span></td>
		<td class="R23C5"><span style="white-space:nowrap;max-width:0px;"><?= number_format($nds, 2, '.', ' ')?></span></td>
		<td><span></span></td>
		<td></td>
	</tr>
	<tr class="R4">
		<td class="R23C5" colspan="6"><span style="white-space:nowrap;max-width:0px;">Всего&nbsp;к&nbsp;оплате:</span></td>
		<td class="R23C5"><span style="white-space:nowrap;max-width:0px;"><?= number_format($payment->value, 2, '.', ' ') ?></span></td>
		<td><span></span></td>
		<td></td>
	</tr>
	</tbody></table>
<table style="width:100%; height:0px; " cellspacing="0">
	<colgroup><col width="7">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="17">
		<col width="16">
		<col width="17">
		<col width="17">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col width="21">
		<col>
	</colgroup><tbody><tr class="R4">
		<td><span></span></td>
		<td class="R26C1" colspan="32"><span style="white-space:nowrap;max-width:0px;">Всего&nbsp;наименований&nbsp;1,&nbsp;на&nbsp;сумму&nbsp;<?= number_format($payment->value, 2, '.', ' ') ?>&nbsp;руб.</span></td>
		<td></td>
	</tr>
	<tr class="R4">
		<td><span></span></td>
		<td class="R27C1" colspan="31"><?= num2str($payment->value) ?></td>
		<td><span></span></td>
		<td></td>
	</tr>
	<tr class="R14">
		<td><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td class="R14C1"><div style="position:relative; height:9px;width: 100%; overflow:hidden;"><span></span></div></td>
		<td><div style="width:100%;height:9px;overflow:hidden;">&nbsp;</div></td>
	</tr>
	<tr>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td><span></span></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="R4">
		<td><span></span></td>
		<td class="R30C1" colspan="5"><span style="white-space:nowrap;max-width:0px;">Руководитель</span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C10" colspan="9"><span style="white-space:nowrap;max-width:0px;">Фазлыев&nbsp;Р.Ф.</span></td>
		<td><span></span></td>
		<td class="R30C1" colspan="4"><span style="white-space:nowrap;max-width:0px;">Бухгалтер</span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C6"><span></span></td>
		<td class="R30C10" colspan="6"><span></span></td>
		<td></td>
	</tr>
	</tbody></table>


<!--</body>-->