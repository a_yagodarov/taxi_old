<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заявок на пополнение счета';
$this->params['breadcrumbs'][] = $this->title;
$statusUrl = \yii\helpers\Url::toRoute('payment/change-status');

$script = <<< JS

$('.status').on('change', function(){
	var status = $(this).val();
	var id = $(this).attr('target_id');
	$.ajax({
		url : '$statusUrl',
		method: "POST",
		data : {
			status : status,
			id : id,
		},
	}).done(function(data){
		console.log(data);
	});
});
JS;

$this->registerJs($script);
?>
<div class="payment-index">

	<h3><?= Html::encode($this->title) ?></h3>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<!--    <p>-->
	<!--        --><?//= Html::a('Create Payment', ['create'], ['class' => 'btn btn-success']) ?>
	<!--    </p>-->
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

//            'id',
			[
				'attribute' => 'client_id',
				'format' => 'raw',
				'value' => function($model)
				{
					$arr = \app\models\Client::getUserInfo($model->client_id);
					$echo = $arr['legal_name'];
					return Html::a($echo, \yii\helpers\Url::toRoute(['client/update', 'id' => $model->client_id]));
				},
				'filter' => Html::activeDropDownList(
					$searchModel,
					'client_id',
					\app\models\Client::getClientsForDropDownList(),
					[
						'prompt' => Yii::t('app', 'Choose'),
						'class' => 'form-control'
					])
			],
			[
				'attribute' => 'link_pdf',
				'format' => 'html',
				'value' => function($model){
					return Html::a('<i class="fa fa-save fa-1" aria-hidden="true"></i> '.'Скачать файл', \yii\helpers\Url::toRoute(['/payment/bill', 'id' => $model->id]));
				}
			],
			'value',
			[
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function($model)
				{
					return Html::dropDownList('drop', $model->status, \app\models\Payment::getStatusList(), [
						'target_id' => $model->id,
						'class' => 'status'
					]);
				}
			],

//			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
