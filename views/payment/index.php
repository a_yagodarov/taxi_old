<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заявок на пополнение счета';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="payment-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Пополнить баланс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
	            'attribute' => 'link_pdf',
	            'format' => 'html',
	            'value' => function($model){
		            return Html::a('<i class="fa fa-save fa-1" aria-hidden="true"></i> '.'Скачать файл', \yii\helpers\Url::toRoute([
			            '/payment/bill',
			            'id' => $model->id
		            ]), [
			            'target' => '_blank'
		            ]);
	            }
            ],
            'value',
	        [
		        'attribute' => 'status',
		        'value' => function($model)
		        {
			        return \app\models\Payment::getStatusById($model->status);
		        }
	        ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
