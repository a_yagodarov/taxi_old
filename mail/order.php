<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 10.01.2017
 * Time: 19:48
 */
/* @var $order app\models\Order
 */


?>
<h3>Заказ №<?= $order->id ?></h3>
<p>Статус: <?= $order->status->name ?></p>
<p>Автомобиль: <?= $order->car->getFullCarName() ?></p>
<p>Водитель: <?= $order->driver->getDriverFullName() ?></p>
