<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "driver".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $middle_name
 * @property integer $town_id
 * @property integer $car_id
 * @property integer $phone_number
 * @property integer $phone_number_2
 * @property string $email
 * @property string $comment
 *
 * @property Car $car
 * @property Town $town
 */
class Driver extends \yii\db\ActiveRecord
{
	public $cars;
	public static $empty = 'Нет';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['town_id', 'car_id', 'phone_number', 'phone_number_2'], 'integer'],
	        [['phone_number', 'phone_number_2'], 'match', 'pattern' => '/^([+]?)(7|8|9)\d{9,11}$/i'],
            [['surname', 'name', 'middle_name', 'email'], 'string', 'max' => 64],
            [['comment'], 'string', 'max' => 640],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['town_id'], 'exist', 'skipOnError' => true, 'targetClass' => Town::className(), 'targetAttribute' => ['town_id' => 'id']],
	        [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    return [
		    'id' => Yii::t('app', 'ID'),
		    'surname' => Yii::t('app/person', 'Surname'),
		    'name' => Yii::t('app/person', 'Name'),
		    'middle_name' => Yii::t('app/person', 'Middle Name'),
		    'town_id' => Yii::t('app', 'Town'),
		    'car_id' => Yii::t('app', 'Car'),
		    'phone_number' => Yii::t('app', 'Phone'),
		    'phone_number_2' => Yii::t('app', 'Phone'),
		    'email' => Yii::t('app', 'Email'),
		    'comment' => Yii::t('app', 'Comment'),
	    ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(Town::className(), ['id' => 'town_id']);
    }

//	public function getCars()
//	{
//		return $this->hasMany(DriverCar::className(), ['driver_id' => 'id']);
//	}

	public function getDriverFullName()
	{
		return $this->getDriversForDropdownList()[$this->id];
	}

	public static function getDriversForDropdownList()
	{
		$result = (new Query())
			->select('id,name,surname')
			->from(Driver::tableName())
			->all();
		$arr = [];
		foreach ($result as $item)
		{
			$arr[$item['id']] = $item['surname'].' '.$item['name'];
		}
		return $arr;
	}

	public function getNameAndNumber()
	{
		return $this->name.' '.$this->middle_name.'<br>'.'<b>Номер:</b>'.$this->phone_number;
	}

	public static function getDriverById($id)
	{
		$drivers = self::getDriversForDropdownList();
		if ($id)
		{
			if (array_key_exists($id, $drivers))
				return self::getDriversForDropdownList()[$id];
		}
		else
		{
			return self::$empty;
		}
		return self::$empty;
	}

	public function loadCars()
	{
		$result = (new Query())
			->select('id')
			->from(DriverCar::tableName())
			->where(['driver_id' => $this->id])
			->all();
		$arr[0] = new DriverCar();
		foreach ($result as $item)
		{
			$arr[$item['id']] = DriverCar::findOne(['id' => $item['id']]);
		}
		$this->cars = $arr;
	}

	public function saveCars($rows = [])
	{
		$query = DriverCar::deleteAll(['driver_id' => $this->id]);
		foreach($rows as $item)
		{
			$model = new DriverCar(['driver_id' => $this->id]);
			$model->car_id = $item['car_id'];
			if ($model->validate())
				$model->save();
		}
	}

	public function getDriverCars($id)
	{
		$arr = [];
		$result = (new Query())
			->select('id,company,model,number')
			->from(Car::tableName())
			->where(['id' => $this->car_id])
			->one();
		$arr[$result['id']] = Car::getCarBrandById($result['company']).' '.$result['model'].' '.$result['number'];
		$result = (new Query())
			->select('car.*')
			->from('car')
			->leftJoin('driver_car', '`driver_car`.`car_id` = `car`.`id`')
			->where(['driver_car.driver_id' => $this->id])
			->all();
		if (is_array($result))
		{
			foreach($result as $item)
			{
				$arr[$item['id']] = Car::getCarBrandById($item['company']).' '.$item['model'].' '.$item['number'];
			}
			return $arr;
		}
		else
		{
			return $arr;
		}
	}
}
