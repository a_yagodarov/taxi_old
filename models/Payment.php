<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $link_pdf
 * @property integer $status
 * @property integer $value
 */
class Payment extends \yii\db\ActiveRecord
{
//	public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'status', 'value'], 'integer'],
	        [['value'], 'required', 'on' => 'create']
        ];
    }

	public static function getStatusList()
	{
		return [
			'0' => 'В обработке',
			'1' => 'Принято',
			'2' => 'Не принято',
//			'3' => 'Отменено'
		];
	}


	public static function getStatusById($id)
	{
		return self::getStatusList()[$id];
	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'link_pdf' => 'Счет на оплату',
//            'file' => 'Файл в формате pdf',
            'status' => 'Статус',
            'value' => 'Зачисляемая сумма',
        ];
    }

	public function changeStatus($status)
	{
		if ($this->status == 1 && ($status == 0 || $status == 2))
		{
			$this->status = $status;
			$this->save();
			return $this->decreaseBalance();
		}
		elseif(($this->status == 0 || $this->status == 2) && $status == 1)
		{
			$this->status = $status;
			$this->save();
			return $this->increaseBalance();
		}
	}

	public function increaseBalance()
	{
		$model = UserInfo::findOne(['id' => $this->client_id]);
		$model->balance += $this->value;
		return $model->save();
	}

	public function decreaseBalance()
	{
		$model = UserInfo::findOne(['id' => $this->client_id]);
		$model->balance = $model->balance - $this->value;
		return $model->save();
	}
}
