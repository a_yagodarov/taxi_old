-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Янв 09 2017 г., 23:28
-- Версия сервера: 5.5.52-38.3
-- Версия PHP: 5.6.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `innovaru_ztu`
--

-- --------------------------------------------------------

--
-- Структура таблицы `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `from_street` varchar(64) DEFAULT NULL,
  `to_street` varchar(64) DEFAULT NULL,
  `from_home` varchar(16) DEFAULT NULL,
  `to_home` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `address`
--

INSERT INTO `address` (`id`, `order_id`, `from_street`, `to_street`, `from_home`, `to_home`) VALUES
(1, 5, '', '', '', ''),
(2, 6, 'гостиница', '', '', ''),
(3, 7, '', '', '', ''),
(4, 8, '', '', '', ''),
(5, 9, '', '', '', ''),
(6, 10, '', '', '', ''),
(7, 11, 'с.  Булгаково', '50 лет Октября', '', '17'),
(8, 12, 'с.  Булгаково', '50 лет Октября', '', '17'),
(9, 13, '', '', '', ''),
(10, 14, '', '', '', ''),
(11, 15, 'с.  Булгаково', '50 лет Октября', '', '17'),
(12, 16, '', '50 лет Октября ', '', '17'),
(13, 17, '', '50 лет Октября ', '', '17'),
(14, 18, '', '', '', ''),
(15, 19, '', '', '', ''),
(16, 20, '', '', '', ''),
(17, 21, '', '', '', ''),
(18, 22, '50 лет Октября ', 'Привокзальная площадь', '17', '3');

-- --------------------------------------------------------

--
-- Структура таблицы `car`
--

CREATE TABLE IF NOT EXISTS `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` smallint(6) DEFAULT NULL,
  `model` varchar(24) DEFAULT NULL,
  `class` smallint(6) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `number` varchar(24) DEFAULT NULL,
  `color` varchar(24) DEFAULT NULL,
  `number_of_passengers` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `car`
--

INSERT INTO `car` (`id`, `company`, `model`, `class`, `year`, `number`, `color`, `number_of_passengers`) VALUES
(9, 3, 'ix35', 2, 2015, 'С053ОВ102', 'белый', 3),
(10, 5, 'Superb ', 3, 2012, 'В405УТ102', 'серый', 3),
(11, 3, 'H1', 6, 2012, 'О322ОТ102', 'серый', 9),
(14, 8, 'Rio', 1, 2014, 'С598ВЕ102', 'белый', 3),
(15, 9, 'Almera', 1, 2015, 'С569СР102', 'серебристый', 3),
(16, 4, 'Caravelle', 6, 2013, 'Х515ЕЕ102', ' чёрный', 7),
(18, 10, 'Kuga', 2, 2014, 'Р091НМ102', 'белый', 3),
(21, 5, 'Octavia', 2, 2013, 'Р522АР102', 'серый', 3),
(23, 5, 'Octavia', 2, 2012, 'Н369МТ102', 'серый', 3),
(24, 5, 'Octavia', 2, 2013, 'Р103СК102', 'белый', 3),
(25, 3, 'Solaris', 1, 2011, ' С058ТО102', 'фиолетовый', 3),
(26, 3, 'Solaris', 1, 2014, 'С496ВМ102', 'белый', 3),
(29, 5, 'Octavia', 2, 2012, ' С247РР102', 'белый', 3),
(31, 5, 'Octavia', 2, 2011, 'Е070НТ102', 'чёрный', 3),
(33, 8, 'Optima', 3, 2012, 'О365ОС102', 'золотой', 3),
(34, 10, 'Mondeo', 3, 2012, 'Р464НХ102', 'белый', 3),
(35, 4, 'Transporter', 6, 2012, ' К683СК102', 'чёрный', 6),
(36, 4, 'Polo', 1, 2012, 'О419ЕС102', 'серебристый', 3),
(38, 11, 'Camry', 3, 2012, 'М737ХЕ102', 'белый', 3),
(40, 9, 'TEANA', 2, 2007, 'Н858СМ102', 'Серый', 4),
(41, 14, 'RX400', 2, 2007, 'М474ЕА102', 'Бежевый', 4),
(42, 4, 'MULTIVAN', 6, 2012, 'О435УК102', 'Черный', 6),
(43, 7, 'OUTLANDER', 3, 2014, 'Р393УА102', 'Белый', 4),
(44, 11, 'Camry', 3, 2015, 'С682ОО102', 'Белый', 4),
(45, 4, 'Tiguan', 2, 2008, 'в835кс102', 'белый', 4),
(46, 11, 'Corolla', 2, 2012, 'о863ре102', 'черный', 4),
(47, 11, 'Corolla', 2, 2012, 'у384ах102', 'белый', 4),
(48, 7, 'outlander', 3, 2014, 'Р666ТТ102', 'белый', 4),
(49, 3, 'SANTA FE', 2, 2008, 'К765ЕО102', 'черный', 4),
(50, 5, 'OCTAVIA', 2, 2005, 'Н049ВТ102', 'синий', 4),
(51, 13, 'klan(универсал)', 1, 2012, 'О680КТ102', 'серебристый', 4),
(52, 5, 'Oktavia', 2, 2014, 'C598АО102', 'серебристый', 4),
(53, 8, 'Cerato', 2, 2012, 'Н133РК102', 'Черный', 3),
(54, 13, 'cruze', 2, 2013, 'р641ва102', 'серебристый', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `car_brand`
--

CREATE TABLE IF NOT EXISTS `car_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `car_brand`
--

INSERT INTO `car_brand` (`id`, `name`) VALUES
(3, 'Hyundai'),
(4, 'Volkswagen'),
(5, 'Skoda'),
(6, 'Mazda'),
(7, 'Mitsubishi'),
(8, 'Kia'),
(9, 'Nissan'),
(10, 'Ford'),
(11, 'Toyota'),
(12, 'Audi'),
(13, 'Chevrolet'),
(14, 'Lexus');

-- --------------------------------------------------------

--
-- Структура таблицы `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `town_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `phone_number` varchar(64) DEFAULT NULL,
  `phone_number_2` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `comment` varchar(640) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `driver_town_id` (`town_id`),
  KEY `driver_car_id` (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `driver`
--

INSERT INTO `driver` (`id`, `surname`, `name`, `middle_name`, `town_id`, `car_id`, `phone_number`, `phone_number_2`, `email`, `comment`) VALUES
(3, 'Звонов', 'Николай', 'Викторович', 1, 36, '79613601732', '', 'zvonov1984@yandex.ru', ''),
(5, 'Чурбаев', 'Ришат', 'Рашитович', 1, 9, '79177799003', '', 'churbaev_rr@mail.ru', ''),
(7, 'Миндияров', 'Радик', 'Мавлетдинович', 1, 35, '79279482345', '', 'minrat@bk.ru', ''),
(8, 'Миннуллин', 'Ринат', 'Илдарович', 1, 21, '79656658265', '', 'rinat-marketing@mail.ru', ''),
(9, 'Миронов', 'Василий', 'Васильевич', 1, 25, '79178073422', '', 'vasilii.vasilevich@mail.ru', ''),
(10, 'Нургалиев', 'Дамир', 'Зулхатович', 1, 11, '79174414700', '', '', ''),
(11, 'Петухов', 'Сергей', 'Евгеньевич', 1, 24, '79272384135', '', 'sergeip93560@gmail.ru', ''),
(12, 'Полозков', 'Андрей', 'Александрович', 1, 18, '79872453441', '', 'and.al.pol@mai.ru', ''),
(13, 'Рахматуллин', 'Роберт', 'Абдуллович', 1, 23, '79273039056', '', 'r.robert1509@gmail.ru', ''),
(14, 'Сайфутдинов', 'Эдуард', 'Ривхатович', 1, 26, '79378469099', '', 'saif2005@yandex.ru', ''),
(15, 'Сиразетдинов', 'Динар', 'Галиевич', 1, 14, '79656501840', '', 'sirazetdinovdinar80@gmail.com', ''),
(16, 'Фаттахов', 'Рамиль', 'Фанисович', 1, 33, '79191443225', '', '', ''),
(17, 'Фахрутдинов', 'Рустам', 'Рашитович', 1, 34, '79649598877', '', '', ''),
(18, '  Щербаков', 'Виктор', 'Борисович', 1, 29, '79273479961', '', '', ''),
(19, 'Юферов', 'Александр', 'Евгеньевич', 1, 15, '79177880099', '', 'alex00713@yandex.ru', ''),
(20, 'Ялаев', 'Виталий', 'Георгиевич', 1, 10, '79177887676', '', 'vitaliy-citi@yandex.ru', ''),
(21, 'Буляков', 'Рамис', 'Рифович', 1, 38, '79196169933', '', '', ''),
(25, 'Косневич', 'Андрей', 'Владимирович', 1, 40, '79174310563', '', 'kadr1708@mail.ru', ''),
(26, 'Бурхантдинов', 'Ильшат', 'Радикович', 1, 41, '79177788550', '', '', ''),
(27, 'Хаванский', 'Вадим', 'Юрьевич', 1, 42, '79018100943', '', '', ''),
(28, 'Бикташев', 'Азат', 'Гарифьянович', 1, 43, '79374866648', '', 'elvira1905@mail.ru', ''),
(29, 'Гумеров', 'Ришат', 'Вагизович', 1, 44, '79174413394', '', '', ''),
(30, 'Тугузбаев', 'Ильгиз', 'Маратович', 1, 45, '89279479277', '', '', ''),
(31, 'Сидоров', 'Александр', 'Владимирович', 1, 46, '89871080639', '', '', ''),
(32, 'Туктамышев', 'Тагир', 'Азатович', 1, 47, '89876273663', '', 'tagir_tagir_a@mail.ru', ''),
(33, 'Мухаметов', 'Анвар', 'Абузарович', 1, 48, '89174392450', '', 'anvar454@mail.ru', ''),
(34, 'Орлов', 'Артем', 'Иванович', 1, 49, '89608001783', '', 'raven1984@mail.ru', ''),
(35, 'Дорофеев', 'Эдуард', 'Александрович', 1, 50, '89872592136', '', 'keyhen.ed@gmeil.com', ''),
(36, 'Мельников', 'Александр', 'Вячеславович', 1, 51, '89191479457', '', '777melnik777@vfil.ru', ''),
(37, 'Сидоров', 'Владислав', 'Васильевич', 1, 52, '89273164316', '', '', ''),
(38, 'Фазлыев', 'Руслан', 'Фанилевич', 1, 53, '89273161630', '', '', ''),
(39, 'Афанасенко', 'Алексей', 'Робертович', 1, 54, '89656555268', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `driver_car`
--

CREATE TABLE IF NOT EXISTS `driver_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `driver_id_id` (`driver_id`),
  KEY `car_id_id` (`car_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `driver_car`
--

INSERT INTO `driver_car` (`id`, `driver_id`, `car_id`) VALUES
(28, 25, 40),
(5, 7, 35),
(3, 2, 32),
(4, 4, 12),
(6, 7, 16),
(17, 13, 23),
(8, 20, 10),
(9, 3, 36),
(10, 5, 9),
(11, 6, 17),
(12, 8, 21),
(13, 9, 25),
(14, 10, 11),
(15, 11, 24),
(16, 12, 18),
(18, 14, 26),
(19, 15, 14),
(20, 16, 33),
(21, 17, 34),
(22, 18, 29),
(23, 19, 15),
(24, 21, 38),
(25, 22, 31),
(26, 24, 39),
(27, 23, 37),
(29, 26, 41),
(30, 27, 42),
(31, 28, 43),
(32, 29, 44),
(33, 30, 45),
(34, 31, 46),
(35, 32, 47),
(37, 33, 48),
(38, 34, 49),
(39, 35, 50),
(40, 36, 51),
(41, 37, 52),
(42, 38, 53),
(43, 39, 54);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1460995417),
('m140209_132017_init', 1460995422),
('m140403_174025_create_account_table', 1460995422),
('m140504_113157_update_tables', 1460995424),
('m140504_130429_create_token_table', 1460995425),
('m140830_171933_fix_ip_field', 1460995425),
('m140830_172703_change_account_table_name', 1460995425),
('m141222_110026_update_ip_field', 1460995425),
('m141222_135246_alter_username_length', 1460995425),
('m150614_103145_update_social_account_table', 1460995426),
('m150623_212711_fix_username_notnull', 1460995426),
('m151218_234654_add_timezone_to_profile', 1460995427),
('m160418_161849_add_user_role_id', 1461060802),
('m160419_103046_add_phone_user', 1461061903),
('m160419_110935_create_car_table', 1461064625),
('m160419_151443_create_towns_table', 1461079035),
('m160419_165936_add_way_tables', 1461086311),
('m160419_190453_create_tariff_table', 1461093460),
('m160420_141344_create_point_table', 1461162532),
('m160420_150225_create_tariff_min_table', 1461165138),
('m160421_131919_add_point_relation', 1461244861),
('m160421_133715_change_way_to_transfer', 1461246065),
('m160426_075404_create_driver_table', 1461659463),
('m160426_124830_add_user_admin_id', 1461675252),
('m160427_125136_create_car_brand_table', 1461761595),
('m160428_112454_change_foreign_to_point', 1462045738),
('m160503_131327_create_user_info_table', 1462494506),
('m160504_104039_create_order_table', 1462494506),
('m160504_150056_create_status_table', 1462494506),
('m160505_145438_add_status_fields', 1462494506),
('m160505_181818_add_foreign_order', 1462494506),
('m160505_192859_insert_manager_row', 1462494506),
('m160506_154848_lol', 1462576451),
('m160506_155812_add_driver_foreign_key', 1462576452),
('m160508_152940_add_foreign_key_user_info', 1462754764),
('m160509_001046_add_balance_to_client', 1462754764),
('m160510_185250_add_driver_cars_table', 1463064001),
('m160511_223933_add_house_street_field', 1463064001),
('m160512_113124_add_address_to_point', 1463064001),
('m160512_194449_create_payment_table', 1463423329),
('m160516_130313_add_payment_value', 1463423329),
('m160516_210353_add_order_price_column', 1463437478),
('m160518_093838_change_fields_into_string', 1463570285),
('m160518_202151_add_kpp_field_into_user_info', 1463654142);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `datetime_booking` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `from_point_id` int(11) DEFAULT NULL,
  `to_point_id` int(11) DEFAULT NULL,
  `text_table` varchar(256) DEFAULT NULL,
  `car_class_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `passengers` int(11) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_from_point_id` (`from_point_id`),
  KEY `order_to_point_id` (`to_point_id`),
  KEY `order_car_id` (`car_id`),
  KEY `order_tariff_id` (`tariff_id`),
  KEY `order_status_id` (`status_id`),
  KEY `order_client_id` (`client_id`),
  KEY `order_driver_id` (`driver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `date_create`, `date_update`, `datetime_booking`, `status_id`, `from_point_id`, `to_point_id`, `text_table`, `car_class_id`, `car_id`, `tariff_id`, `passengers`, `comment`, `client_id`, `driver_id`, `price`) VALUES
(11, NULL, NULL, '2016-12-12 03:40:00', 4, 6, 8, NULL, NULL, 9, 2, 3, 'Необходима табличка с именем Chen Si. Международный терминал, номер рейса SU-1238 , контактный номер телефона гостя Chen Si 89651518141.', 17, 5, NULL),
(6, NULL, NULL, '2016-04-28 07:00:00', 4, 6, 3, NULL, NULL, 35, 5, 7, 'Убывают поездом в 07:04 по Московскому времени', 15, 7, NULL),
(10, NULL, NULL, '2016-12-09 14:00:00', 4, 8, 6, NULL, NULL, 15, 2, 2, 'Егоров Денис и Пашин Михаил тел. гостя 89515759707.', 17, 19, NULL),
(12, NULL, NULL, '2016-12-12 03:40:00', 4, 6, 8, NULL, NULL, 41, 2, 3, 'Необходима табличка с именем Chen Si. Международный терминал, номер рейса SU-1238 , контактный номер телефона гостя Chen Si 89651518141.', 17, 26, NULL),
(13, NULL, NULL, '2016-12-11 06:00:00', 4, 10, 6, NULL, NULL, 42, 5, 4, 'Имя Клиента для Ваучера:	Koval Anton\r\nМестное время и дата вылета :	воскресенье 11 декабря 2016 в 08:10\r\nНомер авиарейса:	SU1239\r\nНомер телефона:	+79859293707', 1, 27, NULL),
(14, NULL, NULL, '2016-12-11 11:00:00', 4, 10, 6, NULL, NULL, 43, 2, 3, 'Имя Клиента для Ваучера:	Bugaev Eduard\r\nМестное время и дата вылета :	воскресенье 11 декабря 2016 в 12:55\r\nНомер авиарейса:	SU1231\r\nНомер телефона:	+79049403263', 1, 28, NULL),
(15, NULL, NULL, '2016-12-12 05:05:00', 4, 6, 8, NULL, NULL, 44, 2, 1, 'Пассажир Залите Андрей, тел. +79817440017, рейс FV - 6431 прилетает из Санкт-Петербурга в 5.05. Встретить пассажира с табличкой', 17, 29, NULL),
(16, NULL, NULL, '2016-12-19 18:45:00', 4, 6, 8, NULL, NULL, 9, 2, 1, 'Козловский Максим Викторович (моб. 8 916 311 58 16), SU 1232,прилет в 18:35\r\n', 17, 5, NULL),
(17, NULL, NULL, '2016-12-20 22:10:00', 4, 3, 8, NULL, NULL, 9, 2, 1, 'Гость Дмитрук Ольга Викторовна\r\nпоезд №104*ЖА (скорый) Самара-Уфа, прибытие 20.12.2016 в 20:06 (МСК), вагон №08К.  \r\n№телефона Дмитрук ОВ +7-9022-912-134 ', 17, 5, NULL),
(18, NULL, NULL, '2016-12-21 06:00:00', 4, 8, 6, NULL, NULL, 42, 5, 10, 'Контактный номер телефона : +7 347 226 06 26\r\nПо телефону был подтвержден тариф 1600 руб!:)', 17, 27, NULL),
(19, NULL, NULL, '2016-12-22 01:20:00', 4, 6, 8, NULL, NULL, 9, 2, 1, 'Гость - Пилишкин Александр Геннадьевич\r\nКонтактный телефон - 8 987 290 35 93\r\nНомер рейса 5N-757, Москва - Уфа, прибытие  в 01:10', 17, 5, NULL),
(21, NULL, NULL, '2016-12-26 21:55:00', 4, 6, 8, NULL, NULL, 45, 2, 1, 'Гость - Хаткевич Антон\r\nКонтактный телефон - 8 919 726 14 12\r\nНомер рейса - S7 - 97, Москва-Уфа, прибытие в 21:40', 17, 30, NULL),
(22, NULL, NULL, '2016-12-30 08:00:00', 4, 8, 3, NULL, NULL, 9, 2, 1, 'Едет гость Katayaprath Dhanil, номер телефона 8(347)2260626. Гость нерусскоговорящий.', 17, 5, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `link_pdf` varchar(64) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `payment`
--

INSERT INTO `payment` (`id`, `client_id`, `link_pdf`, `status`, `value`) VALUES
(1, 16, NULL, 1, 100);

-- --------------------------------------------------------

--
-- Структура таблицы `point`
--

CREATE TABLE IF NOT EXISTS `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `street` varchar(64) DEFAULT NULL,
  `home` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `point_town_id` (`town_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `point`
--

INSERT INTO `point` (`id`, `town_id`, `address`, `street`, `home`) VALUES
(3, 1, 'Ж/Д вокзал', NULL, NULL),
(4, 1, 'Южный автовокзал', NULL, NULL),
(5, 1, 'Северный автовокзал', NULL, NULL),
(6, 1, 'Аэропорт Уфа', NULL, NULL),
(7, 1, 'Уфа', NULL, NULL),
(8, 1, ' Hampton by Hilton Ufa', '50 лет Октрября', '17'),
(9, 3, 'город', '', ''),
(10, 4, 'ГЛЦ "Аджигардак"', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `phone`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `name`, `title`) VALUES
(1, 'admin', 'Администратор'),
(2, 'legal_entity', 'Юр.лицо'),
(3, 'manager', 'Менеджер'),
(4, 'accountant', 'Бухгалтер'),
(5, 'dispatcher', 'Диспетчер'),
(6, 'individual', 'Физ.лицо');

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'В обработке'),
(2, 'Принято'),
(3, 'На исполнении'),
(4, 'Выполнено'),
(5, 'Отменено');

-- --------------------------------------------------------

--
-- Структура таблицы `tariff`
--

CREATE TABLE IF NOT EXISTS `tariff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `km_price` int(11) DEFAULT NULL,
  `hour_price` int(11) DEFAULT NULL,
  `min_time` int(11) DEFAULT NULL,
  `extra_race` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tariff`
--

INSERT INTO `tariff` (`id`, `name`, `km_price`, `hour_price`, `min_time`, `extra_race`) VALUES
(2, 'Комфорт', 22, 440, 2, 220),
(3, 'Бизнес', 25, 600, 2, 400),
(4, 'VIP', 30, 900, 2, 450),
(5, 'Минивэн', 32, 1050, 2, 525);

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(1, 'PCKC5GIrAvOrcyzF93BaghqjQkmA0GNn', 1461839675, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `town`
--

CREATE TABLE IF NOT EXISTS `town` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `town`
--

INSERT INTO `town` (`id`, `name`) VALUES
(1, 'Уфа'),
(2, 'Стерлитамак'),
(3, 'Набережные Челны'),
(4, 'Аша');

-- --------------------------------------------------------

--
-- Структура таблицы `transfer`
--

CREATE TABLE IF NOT EXISTS `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_from_id` int(11) DEFAULT NULL,
  `town_to_id` int(11) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `town_from_town_id` (`town_from_id`),
  KEY `town_to_town_id` (`town_to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transfer`
--

INSERT INTO `transfer` (`id`, `town_from_id`, `town_to_id`, `distance`) VALUES
(21, 3, 6, 25),
(22, 6, 3, 25),
(23, 7, 6, 50),
(24, 4, 6, 25),
(25, 8, 6, 20),
(26, 6, 8, 20),
(27, 7, 9, 300),
(28, 6, 9, 320),
(29, 10, 6, 100),
(30, 6, 10, 100);

-- --------------------------------------------------------

--
-- Структура таблицы `transfer_tariff_min`
--

CREATE TABLE IF NOT EXISTS `transfer_tariff_min` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_id` int(11) DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transfer_tariff_min`
--

INSERT INTO `transfer_tariff_min` (`id`, `transfer_id`, `tariff_id`, `price`) VALUES
(10, 21, 5, 1850),
(11, 23, 5, 1850),
(12, 22, 5, 1850),
(13, 24, 2, 750),
(14, 25, 2, 750),
(15, 25, 3, 900),
(16, 25, 5, 1850),
(17, 26, 2, 750),
(18, 26, 3, 900),
(19, 26, 5, 1850),
(20, 27, 5, 10000),
(21, 28, 5, 10000),
(22, 29, 2, 2400),
(23, 29, 5, 3150),
(24, 30, 2, 2400),
(25, 30, 5, 3150);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT '2',
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`),
  UNIQUE KEY `user_unique_username` (`username`),
  KEY `user_admin_id` (`admin_id`),
  KEY `user_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `role_id`, `admin_id`) VALUES
(1, 'admin', 'info@transfer-ufa.ru', '$2y$13$hKrXFNJSjN.Lc.mW5fIEB.68t.FNjjGmW3KZls5QMbRObrL59G/0q', '', 1, NULL, NULL, NULL, 0, 0, 0, 1, NULL),
(13, 'test', 'alexeiyag@gmail.com', '$2y$10$e77fIsTLXrX2NP0xQfscmuXE3zL1fdSvtN7OyHwm4Hyr2fZCkouGq', 'l3dx8fWAFJ28j6PWqYGJEwAJTkDDK_7i', 1461839714, NULL, NULL, '66.249.81.156', 1461839714, 1461839723, 0, 4, 1),
(15, 'hotelairportufa', 'garaev@airportufa.ru', '$2y$10$.a.TdwO.tYvjjH8srY.jl.1/.lo5w3uL9S5VgS26eNIZJecZ0K3CS', 'N1mAyFXAizmyEhKjP5QKbveeu4jy4qmn', NULL, NULL, NULL, '92.53.96.15', 1463483451, 1463664360, 0, 2, 1),
(16, 'renova', 'info@renova-ufa.ru', '$2y$10$ptR/xpVUNuWQ0yBgvr.f..DXLnvfMhc9ufHmuL8VO3uXDqRNrJxcy', 'nupYk-F_22oOzogxZFdSYJARC70-Fska', NULL, NULL, NULL, '92.53.96.15', 1463484401, 1463662874, 0, 2, 1),
(17, 'ufahampton@gc-osnova.ru', 'ufahampton@gc-osnova.ru', '$2y$10$0mCuub8KC9TSZkyghKlZAOykZ6OJsHa4wXXDrQuLKfiHOtl/6o.1W', 'krbsDjvdqvfIEdD_RDLf-9kFlHSFxkz0', NULL, NULL, NULL, '95.78.231.103', 1481264021, 1481264021, 0, 2, 1),
(18, 'Azat', 'elvira1905@mail.ru', '$2y$10$ZQj4IqJ/wsuygZAlJBBjOeqEhN91ANiQpYitSWBbULY3EOwWJCisG', 'hQVGYYUTjpGla4tRfujV4grJlzj5Ld4f', 1481618526, NULL, NULL, '145.255.8.68', 1481618526, 1481618526, 0, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `legal_name` varchar(256) DEFAULT NULL,
  `inn` varchar(64) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `responsible` varchar(128) DEFAULT NULL,
  `contact_number` varchar(64) DEFAULT NULL,
  `mailing_address` varchar(256) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `kpp` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_info`
--

INSERT INTO `user_info` (`id`, `title`, `legal_name`, `inn`, `address`, `responsible`, `contact_number`, `mailing_address`, `balance`, `kpp`) VALUES
(17, 'Hampton by Hilton Ufa', 'ООО «Основа-Уфа»', '3664113670', '450005, г. Уфа, ул. 50-летия Октября, д.17', 'Отдел СПиР', '83472260626', '450005, г. Уфа, ул. 50-летия Октября, д.17', 0, '027801001'),
(15, 'Отель Аэропорт Уфа', 'АО "Международный аэропорт Уфа"', '0274108180', '450056, РБ, Уфимский район, с. Булгаково, мкр. Аэропорт', 'Гараев Руслан Рашитович', '73472295224', '450056, РБ, Уфимский район, с. Булгаково, мкр. Аэропорт', 0, '027401001'),
(16, 'Ренова', 'ООО "Ренова"', '0245026750', 'РБ, Уфимский район, с. Булгаково, ул. Цюрупы, 99', 'Руслан', '79273161630', '450501, РБ, Уфимский район, с. Булгаково, а/я 157', 100, '024501001');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `driver`
--
ALTER TABLE `driver`
  ADD CONSTRAINT `driver_car_id` FOREIGN KEY (`car_id`) REFERENCES `car` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `driver_town_id` FOREIGN KEY (`town_id`) REFERENCES `town` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `point`
--
ALTER TABLE `point`
  ADD CONSTRAINT `point_town_id` FOREIGN KEY (`town_id`) REFERENCES `town` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `transfer`
--
ALTER TABLE `transfer`
  ADD CONSTRAINT `point_from_id` FOREIGN KEY (`town_from_id`) REFERENCES `point` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `point_to_id` FOREIGN KEY (`town_to_id`) REFERENCES `point` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
